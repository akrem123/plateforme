const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateAgenceInput(data) {
    let errors = {};
  
    data.nameAg = !isEmpty(data.nameAg) ? data.nameAg : "";
    
    data.cityAg = !isEmpty(data.cityAg) ? data.cityAg : "";

    data.imgUrl = !isEmpty(data.imgUrl) ? data.imgUrl : "";
    
  
    //Title
    if (!Validator.isLength(data.nameAg, { min: 5, max: 30 })) {
      errors.nameAg = "name must be between 5 and 30 characters";
    }
  
    if (Validator.isEmpty(data.nameAg)) {
      errors.nameAg = "name is required";
    }
  
    
    //City
    if (!Validator.isLength(data.cityAg, { min: 2, max: 30 })) {
      errors.cityAg = "city must be between 2 and 30 characters";
    }
  
    if (Validator.isEmpty(data.cityAg)) {
      errors.cityAg = "city is required";
    }
    
    //imgUrl
  if (Validator.isEmpty(data.imgUrl)) {
    errors.imgUrl = "imgUrl is required";
  }

  //imgUrl
  if (!isEmpty(data.imgUrl)) {
    if (!Validator.isURL(data.imgUrl)) {
      errors.imgUrl = "Not a valid URL";
    }
  }

  
    return {
      errors,
      isValid: isEmpty(errors)
    };
  };
  