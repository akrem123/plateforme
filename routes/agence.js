const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const passport = require("passport");

//Load Agence model
const Agence = require("../models/Agence");

//Load Agence validator
const validateAgenceInput = require("../validation/agence");

//add new Agence
//@Route /api/agence/add
router.post(
    "/add",
    passport.authenticate("jwt", { session: false }),
    async (req, res) => {
      const { errors, isValid } = validateAgenceInput(req.body);
  
      if (!isValid) {
        return res.status(400).send(errors);
      }
  
      const AgenceDetails = {
        user: req.user.id,
        nameAg: req.body.nameAg,
        imgUrl: req.body.imgUrl,
        cityAg: req.body.cityAg
      };
  
      const agence = await Agence.findOne({ nameAg: AgenceDetails.nameAg });
  
      if (agence) {
        return res
          .status(400)
          .send({ nameAg: "agence should have unique name." });
      } else {
        const newAgence = await new Agence(AgenceDetails).save();
  
        res
          .status(200)
          .send({ newAgence, msg: "Agence added successfully!" });
      }
    }
  );



  //delete agence
//@Route /api/agence/delete
router.delete(
    "/delete",
    passport.authenticate("jwt", { session: false }),
    async (req, res) => {
      const message = await Agence.findByIdAndDelete({ _id: req.query.id });
  
      if (message) {
        res.status(200).send({ msg: "deleted" });
      } else {
        res.status(400).send({ msg: "not found" });
      }
    }
  );

  //get all agencies
//@Route /api/agence
router.get("/all", async (req, res) => {
  const currentPage = req.query.currentPage;
  const pageSize = req.query.pageSize;
  const filter = req.query.selectedFilter;
  // console.log("page size", currentPage, pageSize);

  if (filter === "sousse" || filter === "mahdia" || filter === "monastir") {
    const agenciesList = await Agence.find({ status: filter })
      .skip((currentPage - 1) * pageSize)
      .limit(pageSize * 1)
      .sort({ date: -1 })
      .populate("user", ["-password"]);

    if (agenciesList.length > 0) {
      res.status(200).send(agenciesList);
    } else {
      res.status(400).send({ err: "not found" });
    }
  }
  if (filter === "all") {
    const agenciesList = await Agence.find()
      .skip((currentPage - 1) * pageSize)
      .limit(pageSize * 1)
      .sort({ date: -1 })
      .populate("user", ["-password"]);
    res.status(200).send(agenciesList);
  }
});

//get agence with id
//@Route /api/agence/:id

router.get("/:id", async (req, res) => {
  if (mongoose.Types.ObjectId.isValid(req.params.id)) {
    const agenceDetail = await Agence.findOne({
      _id: req.params.id
    }).populate("user", ["-password"]);
    console.log(agenceDetail);

    if (agenceDetail) {
      return res.status(200).send(agenceDetail);
    } else {
      return res.status(400).send({ msg: "not found" });
    }
  } else {
    res.status(400).send("Not found");
  }
});

//get total agence count
//@Route /api/agence/
router.get("/", async (req, res) => {
  if (req.query.filter === "sousse" || req.query.filter === "mahdia" || req.query.filter === "monastir") {
    const totalCount1 = await Agence.find({
      status: req.query.filter
    }).countDocuments();
    res.status(200).send({ totalCount1: totalCount1 });
  }
  if (req.query.filter === "all") {
    const totalCount1 = await Agence.find().countDocuments();
    res.status(200).send({ totalCount1: totalCount1 });
  }
});

module.exports = router;