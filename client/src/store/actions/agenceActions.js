import {
    SET_AGENCE,
    SET_ERRORS,
    CLEAR_ERRORS,
    SET_ALL_AGENCIES,
    SET_TOTAL_COUNT1,
    CLEAR_AGENCE,
    SET_PROFILE,
    SET_MESSAGE,
    CLEAR_MESSAGE
  } from "../types";
  import axios from "axios";
  import { getUserAgenceList } from "./profileActions";
  
  export const addAgence = agenceDetails => async dispatch => {
    dispatch({
      type: CLEAR_ERRORS
    });
    dispatch({
      type: CLEAR_MESSAGE
    });
    try {
      const res = await axios.post("/api/agence/add", agenceDetails);
  
      dispatch({
        type: SET_AGENCE,
        payload: res.data.newAgence
      });
      dispatch({
        type: SET_MESSAGE,
        payload: res.data.msg
      });
      dispatch({
        type: CLEAR_MESSAGE
      });
    } catch (err) {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    }
  };
  

  
  export const deleteAgence = id => async dispatch => {
    try {
      await axios.delete("/api/agence/delete", {
        params: {
          id: id
        }
      });
  
      dispatch(getUserAgenceList(1, 5));
    } catch (err) {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    }
  };
  
  export const getAllAgencies = (
    currentPage,
    pageSize,
    selectedFilter
  ) => async dispatch => {
    dispatch({
      type: CLEAR_ERRORS
    });
  
    try {
      const agenciesList = await axios.get("/api/agence/all", {
        params: { currentPage, pageSize, selectedFilter }
      });
  
      const totalCount1 = await axios.get("/api/agence/", {
        params: { filter: selectedFilter }
      });
  
      dispatch({
        type: SET_TOTAL_COUNT1,
        payload: totalCount1.data
      });
  
      dispatch({
        type: SET_ALL_AGENCIES,
        payload: agenciesList.data
      });
    } catch (err) {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    }
  };
  
  export const getAgence = (id, history) => async dispatch => {
    try {
      let agence = await axios.get(`/api/agence/${id}`);
  
      const profile = await axios.get(`/api/profile/${agence.data.user._id}`);
  
      dispatch({
        type: SET_PROFILE,
        payload: profile.data
      });
  
      dispatch({
        type: SET_AGENCE,
        payload: agence.data
      });
    } catch (err) {
      history.push("/not-found");
    }
  };
  
  export const clearAgence = () => dispatch => {
    dispatch({
      type: CLEAR_AGENCE
    });
  };
  