import {
    SET_AGENCE,
    SET_ALL_AGENCIES,
    SET_TOTAL_COUNT1,
    CLEAR_AGENCE
  } from "../types";
  
  const initialState = {
    agence: {},
    agencies: [],
    totalCount1: "",
    loading: false
  };
  
  export default function(state = initialState, action) {
    switch (action.type) {
      case SET_AGENCE:
        return {
          ...state,
          agence: action.payload,
          loading: false
        };
      case SET_ALL_AGENCIES:
        return {
          ...state,
          agencies: action.payload,
          loading: false
        };
      case SET_TOTAL_COUNT1:
        return {
          ...state,
          totalCount1: action.payload.totalCount1
        };
      case CLEAR_AGENCE:
        return {
          agence: {},
          agencies: [],
          totalCount1: "",
          loading: false
        };
      default:
        return state;
    }
  }
  