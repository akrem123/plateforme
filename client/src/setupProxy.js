const proxy = require("http-proxy-middleware");

module.exports = function(app) {
  app.use(proxy("/api/user/*", { target: "http://localhost:5000/" }));
  app.use(proxy("/api/user/property/*", { target: "http://localhost:5000/" }));
  app.use(proxy("/api/user/agence/*", { target: "http://localhost:5000/" }));
  app.use(proxy("/api/profile/*", { target: "http://localhost:5000/" }));
  app.use(
    proxy("/api/user/propertyCount", { target: "http://localhost:5000/" })
  );
  app.use(
    proxy("/api/user/agenceCount", { target: "http://localhost:5000/" })
  );
  app.use(proxy("/api/profile/*", { target: "http://localhost:5000/" }));
  app.use(
    proxy("/api/profile/user/current", { target: "http://localhost:5000/" })
  );

  app.use(proxy("/api/profile/user/*", { target: "http://localhost:5000/" }));
  app.use(
    proxy("/api/profile/user/property/*", { target: "http://localhost:5000/" })
  );
  app.use(
    proxy("/api/profile/user/agence/*", { target: "http://localhost:5000/" })
  );
  app.use(
    proxy("/api/profile/user/property/count", {
      target: "http://localhost:5000/"
    })
  );
  app.use(
    proxy("/api/profile/user/agence/count", {
      target: "http://localhost:5000/"
    })
  );
  app.use(proxy("/api/property/all", { target: "http://localhost:5000/" }));
  app.use(proxy("/api/property/", { target: "http://localhost:5000/" }));
  app.use(proxy("/api/property/add", { target: "http://localhost:5000/" }));
  app.use(proxy("/api/property/update/", { target: "http://localhost:5000/" }));
  app.use(
    proxy("/api/property/delete/*", { target: "http://localhost:5000/" })
  );

  app.use(proxy("/api/agence/all", { target: "http://localhost:5000/" }));
  app.use(proxy("/api/agence/", { target: "http://localhost:5000/" }));
  app.use(proxy("/api/agence/add", { target: "http://localhost:5000/" }));
  app.use(proxy("/api/agence/update/", { target: "http://localhost:5000/" }));
  app.use(
    proxy("/api/agence/delete/*", { target: "http://localhost:5000/" })
  );
};
