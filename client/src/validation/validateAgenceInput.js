import Validator from "validator";
import isEmpty from "./isEmpty";

function validateAgenceInput(data) {
    let errors = {};
  
    data.nameAg = !isEmpty(data.nameAg) ? data.nameAg : "";
    
    
    data.cityAg = !isEmpty(data.cityAg) ? data.cityAg : "";
    
    data.imgUrl = !isEmpty(data.imgUrl) ? data.imgUrl : "";
  
    //Name
    if (!Validator.isLength(data.nameAg, { min: 5, max: 30 })) {
      errors.nameAg = "name must be between 5 and 30 characters";
    }
  
    if (Validator.isEmpty(data.nameAg)) {
      errors.nameAg = "name is required";
    }
  
   
   
  
    // //imgUrl
    if (Validator.isEmpty(data.imgUrl)) {
      errors.imgUrl = "imgUrl is required";
    }
  
    // //imgUrl
    if (!isEmpty(data.imgUrl)) {
      if (!Validator.isURL(data.imgUrl)) {
        errors.imgUrl = "Not a valid URL";
      }
    }
  
    
  
  
  
    //cityAg
    if (!Validator.isLength(data.cityAg, { min: 2, max: 30 })) {
      errors.cityAg = "city must be between 2 and 30 characters";
    }
  
    if (Validator.isEmpty(data.cityAg)) {
      errors.cityAg = "city is required";
    }
  

    return {
      errors,
      isValid: isEmpty(errors)
    };
  }
  
  export default validateAgenceInput;
  