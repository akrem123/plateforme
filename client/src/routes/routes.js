import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import {
  HomePage,
  NotFoundPage,
  PropertyListPage,
  PropertyDetailPage,
  AgenceListPage,
  AgenceDetailPage,
  AgentProfilePage,
  AboutPage,
  ContactPage
} from "../views/common";
import { RegistrationPage, LoginPage } from "../views/visitor";
import {
  AddPropertyPage,
  AddAgencePage,
  Dashboard,
  AgentPropertyListPage,
  AgentAgenceListPage,
  EditPropertyPage,
  EditAgencePage
} from "../views/agent";

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/home" component={HomePage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/contact" component={ContactPage} />

        <Route path="/agent/dashboard" component={Dashboard} />

        <Route path="/agent/properties" component={AgentPropertyListPage} />
        <Route path="/properties-list" component={PropertyListPage} />
        <Route path="/property-detail/:id" component={PropertyDetailPage} />

        <Route path="/agent/agencies" component={AgentAgenceListPage} />
        <Route path="/agencies-list" component={AgenceListPage} />
        <Route path="/agence-detail/:id" component={AgenceDetailPage} />

        <Route path="/agent-profile/:id" component={AgentProfilePage} />

        <Route path="/agent/add-property" component={AddPropertyPage} />
        <Route path="/agent/edit-property/:id" component={EditPropertyPage} />

        <Route path="/agent/add-agence" component={AddAgencePage} />
        <Route path="/agent/edit-agence/:id" component={EditAgencePage} />

        <Route path="/registration" component={RegistrationPage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/not-found" component={NotFoundPage} />
        <Redirect to="/not-found" />
      </Switch>
    );
  }
}

export default Routes;
