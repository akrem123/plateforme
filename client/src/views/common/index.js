import HomePage from "./HomePage/HomePage";
import NotFoundPage from "./NotFoundPage/NotFoundPage";

import PropertyListPage from "./PropertyListPage/PropertyListPage";
import PropertyDetailPage from "./PropertyDetailPage/PropertyDetailPage";

import AgenceListPage from "./AgenceListPage/AgenceListPage";
import AgenceDetailPage from "./AgenceDetailPage/AgenceDetailPage";

import AgentProfilePage from "./AgentProfilePage/AgentProfilePage";
import AboutPage from "./AboutPage/AboutPage";
import ContactPage from "./ContactPage/ContactPage";

export {
  AboutPage,
  ContactPage,
  HomePage,
  NotFoundPage,
  PropertyListPage,
  PropertyDetailPage,
  AgenceListPage,
  AgenceDetailPage,
  AgentProfilePage
};
