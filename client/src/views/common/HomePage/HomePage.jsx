import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../../../store/actions";
import { GoogleMap, CardOne, CardThree } from "../../../components";
import { Spinner } from "reactstrap";

class HomePage extends React.Component {
  componentDidMount() {
    this.props.getAllProperties(1, 10, "all");
    this.props.getAllAgencies(1, 10, "all");
  }

  

  render() {
    let renderComponent;
    const { properties } = this.props.property;

    if (properties === null || Object.keys(properties).length === 0) {
      renderComponent = (
        <div
          style={{ width: "100%", height: "100vh" }}
          className="d-flex align-items-center justify-content-center"
        >
          <Spinner color="primary" />
        </div>
      );
    }
    if (properties.length > 0) {
      renderComponent = properties.slice(0, 3).map(property => {
        return (
          <div key={property._id} className="col-lg-4 col-md-6 col-sm-12 ">
            <CardOne
              propertyId={property._id}
              img={property.imgUrl}
              title={property.title}
              price={property.price}
              area={property.area}
              beds={property.beds}
              baths={property.baths}
              garages={property.garages}
              btnText="View Details"
            />
          </div>
        );
      });
    }
    let renderComponent1;
    const { agencies } = this.props.agence;

    if (agencies === null || Object.keys(agencies).length === 0) {
      renderComponent1 = (
        <div
          style={{ width: "100%", height: "100vh" }}
          className="d-flex align-items-center justify-content-center"
        >
          <Spinner color="primary" />
        </div>
      );
    }
    if (agencies.length > 0) {
      renderComponent1 = agencies.slice(0, 3).map(agence => {
        return (
          <div key={agence._id} className="col-lg-4 col-md-6 col-sm-12 ">
            <CardThree
              agenceId={agence._id}
              img={agence.imgUrl}
              nameAg={agence.nameAg}
              cityAg={agence.cityAg}
              btnText="View Details"
            />
          </div>
        );
      });
    }

    return (
      <React.Fragment>
        <GoogleMap width="100%" height="80vh" />
        <div className="container py-5" style={styles.common}>
          <h1 className="display-4 mb-5 text-center">Properties...</h1>
          <div className="row  m-auto">{renderComponent}</div>
          <div className="text-center my-5">
            <Link to="/properties-list" className="btn btn-primary mt-3">
              More Properties
            </Link>
          </div>
        </div>
        <br/>
        <br/>
        <br/>
        <div className="container py-5" style={styles.common}>
          <h1 className="display-4 mb-5 text-center">Agencies...</h1>
          <div className="row  m-auto">{renderComponent1}</div>
          <div className="text-center my-5">
            <Link to="/agencies-list" className="btn btn-primary mt-3">
              More Agencies
            </Link>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const styles = {
  common: {
    height: "100vh"
  }
};

const mapStateToProps = state => {
  return {
    property: state.property,
    agence: state.agence
  };
};

export default connect(
  mapStateToProps,
  actions
)(HomePage);
