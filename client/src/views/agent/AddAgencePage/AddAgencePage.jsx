import React, { Component } from 'react'
import { toast } from "react-toastify";
import { connect } from "react-redux";
import * as actions from "../../../store/actions";
import { Input, SelectList } from "../../../components/";
import { AgentMenu } from "..";

class AddAgencePage extends Component {

    state = {
        nameAg: "",
        imgUrl: "",
        cityAg: "",
        errors: {}
      };
      handleInputChange = ({ currentTarget }) => {
        const value =
          currentTarget.type === "checkbox"
            ? currentTarget.checked
            : currentTarget.value;
    
        this.setState({
          [currentTarget.name]: value
        });
      };

onFormSubmit = e => {
    e.preventDefault();

    const agenceDetails = {
      nameAg: this.state.nameAg,
      imgUrl: this.state.imgUrl,
      cityAg: this.state.cityAg,
    };

    this.props.addAgence(agenceDetails);
  };

  componentWillUnmount() {
    this.props.clearError();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

    render() {

        if (Object.keys(this.props.message.msg).length > 0) {
            toast.success(this.props.message.msg);
          }

          const options = [
            { label: "Select...", value: "" },
            { label: "SOUSSE", value: "sousse" },
            { label: "MAHDIA", value: "mahdia" },
            { label: "MONASTIR", value: "monastir" }
          ];

        return (
            <div className="container-fluid">
                <div className="row">
          {/* left section */}
          <AgentMenu />
          {/* <!-- right section --> */}
          <div className="m-auto col-lg-8 col-md-8 col-sm-12  p-2">
            {/* <!-- Add New Agence --> */}
            <div className="title text-center display-4 mb-4">
              Add New Agence
            </div>
            <form onSubmit={this.onFormSubmit} className="pb-3">
              <div className="basic-info">
                <strong className="text-muted">Basic information</strong>
                <div className="form-row">
                  <Input
                    classes="col-md-6"
                    label="Name"
                    name="nameAg"
                    placeholder="agence name..."
                    onChange={this.handleInputChange}
                    value={this.state.nameAg}
                    error={this.props.errors.nameAg}
                  />

                  

                  <Input
                    classes="col-md-12"
                    label="Image Url"
                    name="imgUrl"
                    placeholder="agence image..."
                    onChange={this.handleInputChange}
                    value={this.state.imgUrl}
                    error={this.props.errors.imgUrl}
                  />

                 
                </div>
              </div>

              <br />

              <div>
                <div className="form-row">
                 

                </div>
                <div className="form-row">
                 
                  <SelectList
                    label="City"
                    name="cityAg"
                    options={options}
                    onChange={this.handleInputChange}
                    value={this.state.cityAg}
                    error={this.props.errors.cityAg}
                  />
                
                </div>
              </div>

              <br />
              <br />

              <button type="submit" className="btn btn-block btn-primary mt-5">
                Submit
              </button>
            </form>
          </div>
        </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
  return {
    message: state.message,
    errors: state.errors,
    agence: state.agence
  };
};

export default connect(
  mapStateToProps,
  actions
)(AddAgencePage);