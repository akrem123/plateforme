import React from "react";
import { Link } from "react-router-dom";
import { Popup } from "../../../components";

class AgencePageUI extends React.Component {
  render() {
    const { dataList, deleteAgence } = this.props;
    return (
      <div className="row my-5">
        <div className="col">
          <div className="table-responsive">
            <table className="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Agence</th>
                  <th>City</th>
                  <th>More Details</th>
                </tr>
              </thead>
              <tbody>
                {dataList.map(agence => {
                  return (
                    <tr key={agence._id}>
                      <td>
                        <p>
                          {agence.nameAg}
                          {"  "}
                          
                        </p>

                        <img
                          src={agence.imgUrl}
                          style={{ width: "150px", height: "100px" }}
                          className=" img-thumbnail border-0"
                          alt=""
                        />
                      </td>

                      <td>{agence.cityAg}</td>
                      
                      <td>
                        <div className="w-75 d-flex justify-content-around   ">
                          <Link
                            to={`/agent/edit-agence/${agence._id}`}
                            className="btn btn-primary"
                          >
                            <i className="fa fa-edit" /> Edit
                          </Link>

                          <Popup
                            buttonLabel="delete"
                            modelTitle={`${agence.nameAg}`}
                            deleteAgence={deleteAgence}
                            id={agence._id}
                          />

                          <Link
                            target="_blank"
                            to={`/agence-detail/${agence._id}`}
                            className="btn btn-success"
                          >
                            <i className="fa fa-eye" /> View
                          </Link>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
          {/* <!--end of .table-responsive--> */}
        </div>
      </div>
    );
  }
}

export default AgencePageUI;
