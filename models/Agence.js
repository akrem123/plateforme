const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const agenceSchema = new Schema ({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    nameAg: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 30
      },
    cityAg: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 30
      },
    
      imgUrl: {
        type: String,
        required: true,
        default: "http://placehold.jp/300x250.png"
      },
    date: {
        type: Date,
        default: Date.now
      }
});

const Agence = mongoose.model("Agence", agenceSchema);

module.exports = Agence;
